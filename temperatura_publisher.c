#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <libgen.h>

#include "cJSON.h"
#include "mosquitto.h"

#include "templib.c"

#define RPI 48
#define HOST "127.0.0.1"
#define PORT  1883
#define KEEPALIVE  120
#define TOPIC "industriales/lab1518/raspi48/temp"
#define CLIENTE_ID "micliente_pub"

/*--------------------------------------------------------*/
/* obtiene la hora y fecha del sistema */
/*--------------------------------------------------------*/
void get_hora_fecha(char *str_time, char* str_date)
{
    struct tm *tm;
    time_t t;


    t = time(NULL);
    tm = localtime(&t);

    strftime(str_time, 20, "%T", tm);
    strftime(str_date, 20, "%F", tm);
}

/*--------------------------------------------------------*/
/* obtiene la dirección IP actual */
/*--------------------------------------------------------*/
void get_IP(char *str_ip)
{
    FILE *f=popen("hostname -I", "r");
    fscanf(f,"%s",str_ip);
    fclose(f);
}

/*--------------------------------------------------------*/
/* A message was successfully published. */
/*--------------------------------------------------------*/
static void on_publish(struct mosquitto *m, void *udata, int m_id) {
    printf("-- published successfully\n");
}

/*--------------------------------------------------------*/
/* Callback for successful connection: add subscriptions. */
/*--------------------------------------------------------*/
static void on_connect(struct mosquitto *m, void *udata, int res) {
    if (res != 0) {
        printf("connection refused\n");
    }
}

/*--------------------------------------------------------*/
/*--------------------------------------------------------*/
/*--------------------------------------------------------*/
main()
{
    char hora[128],fecha[128], ip[128];
    int rpi=RPI;
    int res;
    int payloadlen = 0;
    char clienteID[256];
    char *serial;
    int n_sensores;
    char *nombre_sensor;
 
      
    n_sensores = num_sensores();
    
    printf("El número de sensores es %d \n" , n_sensores);
    
    if(n_sensores==0)
    {
	   printf("No hay sensores que leer\n");
	   exit(-1);
	}
	
	nombre_sensor =  sensor_n(1);
	
	serial = basename(dirname(strdup(nombre_sensor))); // recortamos del path en num. de serie del sensor
	
	printf("Usando el sensor con ROM: %s,  path: %s \n", serial, nombre_sensor);
	
    sprintf(clienteID, "%s%d", CLIENTE_ID, getpid());
    printf("clienteID: %s\n", clienteID);

    mosquitto_lib_init();

    struct mosquitto *m = mosquitto_new(clienteID, true, NULL);

    mosquitto_connect_callback_set(m, on_connect);
    mosquitto_publish_callback_set(m, on_publish);

    res = mosquitto_connect(m, HOST, PORT, KEEPALIVE);

    if(res == MOSQ_ERR_SUCCESS)
        printf("Connection OK\n");
    else
        printf("Connection error!!\n");
   
    mosquitto_loop_start(m);

    while (1)
    {
        int result;
        char *out;
		float temp;
		
		result=lee_temp(nombre_sensor, &temp);
		if(result!=TEMP_OK) 
		{
			printf("Error leyendo sensor: %d\n", result);
			sleep(5);
			continue;
		}
		printf("TEMPERATURA= %4.2f ºC\n", temp);
		
        get_hora_fecha(hora,fecha);
        get_IP(ip);

        cJSON *json=cJSON_CreateObject();
        cJSON_AddNumberToObject(json, "id", rpi);
        cJSON_AddStringToObject(json, "serial", serial);
        cJSON_AddNumberToObject(json, "temp", temp);
        cJSON_AddStringToObject(json, "fecha", fecha);
        cJSON_AddStringToObject(json, "hora", hora);
        cJSON_AddStringToObject(json, "ip", ip);
        out= cJSON_PrintUnformatted(json);

        res = mosquitto_publish(m, NULL, TOPIC, strlen(out), out, 0, false);
        printf("topic: %s\n",TOPIC);
        printf("mensaje:\n%s\n",out);
        printf("long. mensaje: %d\n",strlen(out));
        if (res != MOSQ_ERR_SUCCESS) {
            printf("publish falló\n");
        }


        free(out);
        cJSON_Delete(json);

        sleep(5);
    }

    mosquitto_destroy(m);
    mosquitto_lib_cleanup();
}
