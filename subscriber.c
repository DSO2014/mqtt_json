#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"


#define HOST  "localhost"
#define TOPIC "raspi/+/temp"

#define TAGS 5
#define TAGSARRAY {"id","temp","fecha","hora","ip"}

main()
{
    char comando[1024];
    char mensaje[2048];

    int rpi;
    double temp;

    sprintf(comando,"mosquitto_sub -h %s -t '%s'", HOST, TOPIC);
    FILE* f=popen(comando,"r");

    while (1)
    {
        int result,i;
        cJSON *json;
        char *out;
        if (fscanf(f, "%s", mensaje))  printf("RAW: #%s#\n", mensaje);

        json = cJSON_Parse(mensaje);
        if (!json) {
            printf("Error before: [%s]\n",cJSON_GetErrorPtr());
        }
        else
        {
            cJSON *obj[TAGS];
            char *nombres[TAGS]=TAGSARRAY;

            for(i=0; i<TAGS; i++)
            {
                obj[i]=cJSON_GetObjectItem(json,nombres[i]);
                if(obj[i]==NULL)
                {
                    printf("ERROR, no encuentro %s\n", nombres[i]);
                    break;
                }
            }
            if(i!=TAGS) continue;

            temp=cJSON_GetObjectItem(json,"temp")->valuedouble;

            printf("  Raspi: %d\n", obj[0]->valueint);
            printf("  temp : %f\n", obj[1]->valuedouble);
            printf("  fecha: %s\n", obj[2]->valuestring);
            printf("  hora : %s\n", obj[3]->valuestring);
            printf("  ip   : %s\n\n", obj[4]->valuestring);
            out=cJSON_Print(json);
            cJSON_Delete(json);
            printf("%s\n\n",out);
            free(out);
        }

    }
}
