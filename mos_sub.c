#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"
#include "mosquitto.h"


#define HOST  "localhost"
#define PORT  1883
#define KEEPALIVE  120
#define TOPIC "lugar/planta/+/temperatura"
#define CLIENTE_ID "micliente_sub"


/*-------------------------------------------------------------*/
/* Successful subscription hook. */
/*-------------------------------------------------------------*/
static void on_subscribe(struct mosquitto *m, void *udata, int mid,
                         int qos_count, const int *granted_qos) {
    printf("-- subscribed successfully\n");
}

/*-------------------------------------------------------------*/
/* Callback for successful connection: add subscriptions. */
/*-------------------------------------------------------------*/
static void on_connect(struct mosquitto *m, void *udata, int res) {
    if (res == 0) {             /* success */
        mosquitto_subscribe(m, NULL, TOPIC , 0);
    } else {
        printf("-- connection refused\n");
    }
}

/*-------------------------------------------------------------*/
/* Handle a message that just arrived via one of the subscriptions. */
/*-------------------------------------------------------------*/
static void on_message(struct mosquitto *m, void *udata,
                       const struct mosquitto_message *msg) {
    int result,i;
    cJSON *json, *obj;
    char *out;
    if (msg == NULL) {
        return;
    }
    printf("\n========================\ngot message @ %s: (%d, QoS %d, %s) '%s'\n",
           msg->topic, msg->payloadlen, msg->qos, msg->retain ? "R" : "!r",
           msg->payload);
 
 
    json = cJSON_Parse(msg->payload);
 
    if (!json) {
        printf("Error before: [%s]\n",cJSON_GetErrorPtr());
        return;
    }
    
    // pinta el JSON
    
    out=cJSON_Print(json);  //JSON a cadena de caracteres
    printf("\n------------------------\nJSON:\n%s\n\n",out);  // pinta cadena
    
    
    // extrae diferentes valores del JSON
     printf("\n------------------------\nValores:\n");
    // pinta id
	obj = cJSON_GetObjectItem(json,"id");
    if(!obj)  {
       printf("Error no encuentro id");
       return;
    }
    printf("  id   : %d\n", obj->valueint);
    
    // pinta temperatura
    obj = cJSON_GetObjectItem(json,"temp");
    if(!obj)  {
       printf("Error no encuentro temp");
       return;
    }
    printf("  temp : %f\n", obj->valuedouble);
    
    // pinta fecha
    obj = cJSON_GetObjectItem(json,"fecha");
    if(!obj)  {
       printf("Error no encuentro fecha");
       return;
    }
    printf("  fecha: %s\n", obj->valuestring);
    
    // pinta hora
    obj = cJSON_GetObjectItem(json,"hora");
    if(!obj)  {
       printf("Error no encuentro hora");
       return;
    }
    printf("  hora : %s\n", obj->valuestring);
      
    // pinta ip
    obj = cJSON_GetObjectItem(json,"ip");
    if(!obj)  {
       printf("Error no encuentro ip");
       return;
    }
    printf("  ip   : %s\n", obj->valuestring);
   
    cJSON_Delete(json);
    free(out);
  
    return;
}

/*-------------------------------------------------------------*/
/*-------------------------------------------------------------*/
main()
{
    int res;
   
    char clienteID[2048];

    sprintf(clienteID, "%s%d", CLIENTE_ID, getpid());
    printf("clienteID: %s\n", clienteID);

    mosquitto_lib_init();

    struct mosquitto *m = mosquitto_new(clienteID, true, NULL);

    mosquitto_connect_callback_set(m, on_connect);
    mosquitto_message_callback_set(m, on_message);
    mosquitto_subscribe_callback_set(m, on_subscribe);

    res = mosquitto_connect(m, HOST, PORT, KEEPALIVE);

    if(res == MOSQ_ERR_SUCCESS)
        printf("Connection OK\n");
    else
        printf("Connection error!!\n");

    res= mosquitto_loop_forever(m, 1000, 1000 /* unused */); // llamada bloqueante

    if(res == MOSQ_ERR_SUCCESS)
        printf("loop OK\n");
    else
        printf("loop error!!\n");

    mosquitto_destroy(m);
    mosquitto_lib_cleanup();

}
