# Programación C para comunicación sobre MQTT usando JSON (README) #

### What is this repository for? ###

* Este repositorio tiene ejemplos de uso del protocolo de comunicaciones MQTT, y usa JSON para encapsular la información en los mensajes
* Para usar el protocolo MQTT usaremos el borker mosquitto, y sus herrramientas y librería 
* Para usar JSON desde C, usaremos la librería cJSON
* Version de diciembre de 2016

### Ficheros incluidos: ###

* publisher.c y subscriber.c : programas para enviar y recibir mensajes usando los clientes mosquitto.
* mos_pub.c y mos_sub.c : programas para enviar y recibir mensajes usando el API de la librería C mosquitto.
* temperatura_publisher.c : programa para enviar las lecturas de temperatura que usaremos para recibirlos en nuestro programa que lee temperaturas.
* Makefile: para compilar los programas
* Prestar atención al valor de HOST para poner la dirección del broker MQTT que estemos usando
* En el Makefile podemos añadir fácilmente un nuevo programa a compilar añadiéndolo a la variable EJECUTABLES

### How do I get set up? ###
* Este repositorio se instala con:
```bash
 cd ~
 git clone  https://DSO2014@bitbucket.org/DSO2014/mqtt_json.git
```
* Para instalar el broker de mosquitto (no hace falta que lo hagan todos, el profesor lo hará en el laboratorio en una Rpi que será la que aloje el servidor/broker MQTT que usaremos todos). Se puede instalar con:
```bash
 sudo apt-get install mosquitto 
```
* Para las herramientas cliente (eso si lo necesitamos todos para probar a enviar/recibir mensajes MQTT):
```bash
 sudo apt-get install mosquitto-clients
```
* Para programar en C tendremos que instalarnos la librería mosquitto. Esta versión está adaptada para nuestra Rpi:
```bash
 cd ~
 git clone  https://DSO2014@bitbucket.org/DSO2014/mosquitto.git
```
* También necesitamos la librería cJSON:
```bash
 cd ~
 git clone  https://DSO2014@bitbucket.org/DSO2014/cjson.git
```
* Al final tendremos que compilar desde nuestro directorio mqtt_json (comprobar en Makefile que las variables apuntan a los directorios donde hemos instalado las librerías mosquitto y cJSON). Para compilar las librerías:
```bash
cd ~/mqtt_json
make libraries
```
* Para compilar los programas ejemplo:
```bash
cd ~/mqtt_json
make
```

### Who do I talk to? ###
* Andrés (andres@uma.es)