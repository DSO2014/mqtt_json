#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "cJSON.h"

#define RPI 1
#define HOST "127.0.0.1"

#define TOPIC "raspi/1/temp"


void get_hora_fecha(char *str_time, char* str_date)
{
    struct tm *tm;
    time_t t;


    t = time(NULL);
    tm = localtime(&t);

    strftime(str_time, 20, "%T", tm);
    strftime(str_date, 20, "%F", tm);
}



void get_IP(char *str_ip)
{
    FILE *f=popen("hostname -I", "r");
    fscanf(f,"%s",str_ip);
    fclose(f);
}

main()
{
    char topic[2048], mensaje[2048];
    char hora[100],fecha[100], ip[100];
    int rpi=RPI;
    double temp=25.5;


    while (1)
    {
        int result;
        char *out;



        get_hora_fecha(hora,fecha);
        get_IP(ip);

        cJSON *json=cJSON_CreateObject();
        cJSON_AddNumberToObject(json, "id", rpi);
        cJSON_AddNumberToObject(json, "temp", temp);
        cJSON_AddStringToObject(json, "fecha", fecha);
        cJSON_AddStringToObject(json, "hora", hora);
        cJSON_AddStringToObject(json, "ip", ip);
        out= cJSON_PrintUnformatted(json);

        sprintf(mensaje, "mosquitto_pub -h %s -t '%s' -m '%s'", HOST, TOPIC, out);
        printf("%s\n\n",mensaje);
        system(mensaje);

        //free(out)
        //out=cJSON_Print(json);
        //printf("%s\n\n",out);

        free(out);
        cJSON_Delete(json);


        sleep(5);

    }
}
